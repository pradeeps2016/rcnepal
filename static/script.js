toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

$(document).ready(function () {
    remove_from_master("#id_available_permissions", "#id_permissions");
    $("#send_to_slave").click(function () {
        swap_selected("#id_available_permissions", "#id_permissions");
        $("#id_permissions option").prop('selected', true)
    });

    $("#send_to_master").click(function () {
        swap_selected("#id_permissions", "#id_available_permissions");
         $("#id_permissions option").prop('selected', true)
    })
})

function swap_selected(from, to) {
    let selected = $(from).val();
    selected.forEach(function (value) {
        let selected_element = $(from).find(('option[value="' + value + '"]'));
        $(to).append(selected_element);
    })
}

function remove_from_master(master, slave) {
    let options = $(slave).children()
    for (let i = 0; i < options.length; i++) {
        let option = options[i]
        let value = $(option).attr('value')
        let option_to_remove = $(master).find('option[value="' + value + '"]');
        $(option_to_remove).remove();
    }
}


