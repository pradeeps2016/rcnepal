menus = [
    {
        'text': 'Users',
        'icon': 'user-friends',
        'permission': 'users.view_user',
        'items': [
            {'name': 'users', 'text': 'Users', 'icon': 'bars', 'permission': 'users.view_user'},
            {'name': 'groups', 'text': "Groups", 'icon': 'user-friends', 'permission': 'auth.view_group'},
        ]
    },
{
        'text': 'Media',
        'icon': 'image',
        'permission': 'blog.view_media',
        'items': [
            {'name': 'media', 'text': 'Images', 'icon': 'image', 'permission': 'blog.view_media', 'kwargs': {'media_type': 'image'}},
            {'name': 'media', 'text': 'Documents', 'icon': 'file', 'permission': 'blog.view_media',
             'kwargs': {'media_type': 'document'}},
        ]
    },
    {
        'text': 'Contents',
        'icon': 'link',
        'permission': 'blog.view_post',
        'items': [
            {'name': 'posts', 'text': 'Posts', 'icon': 'bars', 'permission': 'blog.view_post',
             'kwargs': {'post_type': 'post'}},
            {'name': 'posts', 'text': 'Pages', 'icon': 'bars', 'permission': 'blog.view_post',
             'kwargs': {'post_type': 'page'}},
            {'name': 'categories', 'text': 'Categories', 'icon': 'bars', 'permission': 'blog.view_category'},
            {'name': 'authors', 'text': 'Authors', 'icon': 'bars', 'permission': 'blog.view_author'}
        ]
    },
    {
        'text': 'Advertisement',
        'icon': 'users-cog',
        'permission': 'blog.view_adv',
        'items': [
            {'name': 'adlocations', 'text': 'adlocations', 'icon': 'link', 'permission': 'blog.view_adlocation',
             'kwargs': None},
            {'name': 'advs', 'text': 'Advertisements', 'icon': 'link', 'permission': 'blog.view_adv',
             'kwargs': None},

        ]
    },
    {
        'text': 'Settings',
        'icon': 'users-cog',
        'permission': 'blog.view_menu',
        'items': [
            # {'name': 'languages', 'text': 'Languages', 'icon': 'link', 'permission': 'blog.view_language',
            #  'kwargs': None},
            {'name': 'menus', 'text': 'Menus', 'icon': 'link', 'permission': 'blog.view_menu',
             'kwargs': None},
        ]
    }

]
