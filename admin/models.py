from django.db import models


# Create your models here.

class Process(models.Model):
    name = models.CharField(max_length=100)
    pid = models.IntegerField(null=True)
    active = models.BooleanField(default=False)