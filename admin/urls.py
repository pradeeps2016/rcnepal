from django.urls import path

from admin.views import dashboard, categories, authors, adlocations, advs, menus, media
from admin.views import posts


urlpatterns = [
    path('', dashboard.Dashboard.as_view(), name='dashboard'),
    
    path('content/<str:post_type>/', posts.Index.as_view(), name="posts"),
    path('content/<str:post_type>/new/', posts.New.as_view(), name="add_post"),
    path('content/<int:post_id>/change', posts.Change.as_view(), name="change_post"),
    path('content/<int:post_id>/delete', posts.Delete.as_view(), name="delete_post"),

    #
    # path('languages/', languages.Index.as_view(), name="languages"),
    # path('languages/new', languages.New.as_view(), name="add_language"),
    # path('languages/<int:language_id>/change', languages.Change.as_view(), name="change_language"),
    # path('languages/<int:language_id>/delete', languages.Delete.as_view(), name="delete_language"),

    path('categories/', categories.Index.as_view(), name="categories"),
    path('categories/new', categories.New.as_view(), name="add_category"),
    path('categories/<int:category_id>/change', categories.Change.as_view(), name="change_category"),
    path('categories/<int:category_id>/delete', categories.Delete.as_view(), name="delete_category"),

    path('authors/', authors.Index.as_view(), name="authors"),
    path('authors/new', authors.New.as_view(), name="add_author"),
    path('authors/<int:author_id>/change', authors.Change.as_view(), name="change_author"),
    path('authors/<int:author_id>/delete', authors.Delete.as_view(), name="delete_author"),

    path('adlocations/', adlocations.Index.as_view(), name="adlocations"),
    path('adlocations/new', adlocations.New.as_view(), name="add_adlocation"),
    path('adlocations/<int:adlocation_id>/change', adlocations.Change.as_view(), name="change_adlocation"),
    path('adlocations/<int:adlocation_id>/delete', adlocations.Delete.as_view(), name="delete_adlocation"),

    path('advs/', advs.Index.as_view(), name="advs"),
    path('advs/new', advs.New.as_view(), name="add_adv"),
    path('advs/<int:adv_id>/change', advs.Change.as_view(), name="change_adv"),
    path('advs/<int:adv_id>/delete', advs.Delete.as_view(), name="delete_adv"),

    path('menus/', menus.Index.as_view(), name="menus"),
    path('menus/new', menus.New.as_view(), name="add_menu"),
    path('menus/<int:menu_id>/change', menus.Change.as_view(), name="change_menu"),
    path('menus/<int:menu_id>/delete', menus.Delete.as_view(), name="delete_menu"),
    path('menus/<int:menu_id>/', menus.View.as_view(), name="view_menu"),
    path('menu_items/<int:menu_item_id>/delete', menus.DeleteItem.as_view(), name="delete_menu_item"),
    path('menu_items/<int:menu_item_id>/change', menus.ChangeMenuItem.as_view(), name="change_menu_item"),
    path('menu_items/<int:menu_id>/from/page', menus.ItemFromPage.as_view(), name="add_menu_item_page"),
    path('menu_items/<int:menu_id>/from/category', menus.ItemFromCat.as_view(), name="add_menu_item_cat"),
    path('menu_items/<int:menu_id>/from/link', menus.ItemFromLink.as_view(), name="add_menu_item_link"),


    path('media/browser/<str:media_type>/', media.BrowserIndex.as_view(), name='media_browser'),
    path('media/ckupload/', media.CkUpload.as_view(), name='ckupload'),
    path('media/browser/upload/<str:media_type>/', media.BrowserUpload.as_view(), name='browser_upload'),
    path('media/<str:media_type>/', media.Index.as_view(), name='media'),
    path('media/upload/<str:media_type>', media.Upload.as_view(), name="upload_media"),
    path('media/<int:media_id>/delete', media.Delete.as_view(), name="delete_media"),
 ]
