from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View

from admin.forms.authors import AuthorForm
from blog.models import Author


class Index(View):
    @method_decorator(permission_required('blog.view_author'))
    def get(self, request):
        authors = Author.objects.all()
        return render(request, 'authors/index.html', {'authors': authors})


class New(View):
    @method_decorator(permission_required('blog.add_author'))
    def get(self, request):
        form = AuthorForm()
        form.post_type = 'post'
        return render(request, 'authors/new.html', {"form": form})

    @method_decorator(permission_required('blog.add_author'))
    def post(self, request):
        form = AuthorForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, 'Author created successfully.')
            return redirect(reverse('authors'))
        else:
            return render(request, 'authors/new.html', {'form': form})


class Delete(View):
    @method_decorator(permission_required('blog.delete_author'))
    def post(self, request, author_id):
        author = get_object_or_404(Author, pk=author_id)
        author.delete()
        response = {
            'message': "Deleted successfully",
            'id': '{}'.format(author_id),
            'success': True
        }
        return JsonResponse(response)


class Change(View):
    @method_decorator(permission_required('blog.change_author'))
    def get(self, request, author_id):
        author = get_object_or_404(Author, pk=author_id)
        form = AuthorForm(instance=author)
        return render(request, 'authors/new.html', {"form": form})

    @method_decorator(permission_required('blog.change_author'))
    def post(self, request, author_id):
        author = get_object_or_404(Author, pk=author_id)
        form = AuthorForm(data=request.POST, instance=author)
        if form.is_valid():
            form.save()
            return redirect(reverse('authors'))
        else:
            return render(request, 'authors/new.html', {'form': form})
