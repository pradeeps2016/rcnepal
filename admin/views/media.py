from django.contrib import messages
from django.contrib.auth.decorators import permission_required, login_required
from django.http import JsonResponse
from django.shortcuts import redirect, get_object_or_404, render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView

from admin.forms.media import MediaForm, CkUploaderForm
from blog.models import Media


class BrowserIndex(ListView):
    paginate_by = 50
    template_name = 'media/browser.html'

    @method_decorator(permission_required('blog.view_media'))
    def dispatch(self, request, media_type, *args, **kwargs):
        self.queryset = Media.objects.filter(media_type=media_type).order_by("-id")
        return_type = request.GET.get('return')
        target = request.GET.get('target')
        ckeditor = request.GET.get('ckeditor')
        cke_func_num = request.GET.get('CKEditorFuncNum')
        form = MediaForm()
        self.extra_context = {
            'media_type': media_type,
            'return': return_type,
            'target': target,
            'ckeditor': ckeditor,
            'form': form,
            'url': reverse('browser_upload', kwargs={'media_type': media_type}),
            'cke_func_num': cke_func_num
        }
        response = super(BrowserIndex, self).dispatch(request, args, kwargs)
        return response


class Index(ListView):
    paginate_by = 50
    template_name = 'media/index.html'

    @method_decorator(permission_required('blog.view_media'))
    def dispatch(self, request, media_type, *args, **kwargs):
        self.queryset = Media.objects.filter(media_type=media_type).order_by("-id")
        form = MediaForm()
        self.extra_context = {
            'media_type': 'media_type',
            'form': form,
            'url': reverse('upload_media', kwargs={'media_type': media_type})
        }
        response = super(Index, self).dispatch(request, args, kwargs)
        return response


class Upload(View):
    @method_decorator(permission_required('blog.add_media'))
    def post(self, request, media_type):
        form = MediaForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Uploaded successfully.")
        else:
            messages.error(request, "This file couldn't be uploaded. Please check details.")

        return redirect(reverse('media', kwargs={'media_type': media_type}))


class BrowserUpload(View):
    @method_decorator(permission_required('blog.add_media'))
    def post(self, request, media_type):
        form = MediaForm(data=request.POST, files=request.FILES)
        media_type = request.POST.get('media_type')
        if form.is_valid():
            form.save()
            messages.success(request, "Uploaded successfully.")
        else:
            messages.error(request, "This file couldn't be uploaded. Please check details.")

        return redirect(reverse('media_browser', kwargs={'media_type': media_type}))


class Delete(View):
    @method_decorator(permission_required('blog.delete_media'))
    def post(self, request, media_id):
        media = get_object_or_404(Media, pk=media_id)
        media.delete()
        response = {
            'message': "Deletion Successful",
            'success': True,
            'id': media_id
        }
        return JsonResponse(response)


class CkUpload(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CkUpload, self).dispatch(*args, **kwargs)

    def post(self, request):
        form = CkUploaderForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            file = form.cleaned_data.get('upload')
            media = Media(file=file)
            media.save()
            response = {
                'uploaded': 1,
                'fileName': media.title,
                'url': media.file.url
            }
            return JsonResponse(response)

