from django.utils.decorators import method_decorator
from django.views import View
from django import forms
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import login as auth_login, authenticate as auth_user

from users.models import User



class LoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'password']
        widgets = {
            'password': forms.PasswordInput()
        }


class Dashboard(View):
    @method_decorator(login_required)
    def get(self, request):
        return render(request, 'admin/index.html')


def login_create(request):
    form = LoginForm()
    email = request.POST.get('email')
    password = request.POST.get('password')
    user = auth_user(email=email, password=password)
    if user is not None:
        auth_login(request, user, 'django.contrib.auth.backends.ModelBackend')
        messages.success(request, 'Login successful.')
        return redirect('/admin/')
    else:
        messages.error(request, 'Login failed.')
        return render(request, 'login/new.html', {'form': form})


def login(request):
    form = LoginForm()
    return render(request, 'login/new.html', {'form': form})
