from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View

from admin.forms.menus import MenuForm, MenuItemFromCategoryForm, MenuItemFromPageForm, MenuItemLinkForm, MenuItemForm
from blog.models import Menu, MenuItem, Post, Category


class Index(View):
    @method_decorator(login_required)
    def get(self, request):
        menus = Menu.objects.all()
        return render(request, 'menus/index.html', {'menus': menus})


class New(View):
    @method_decorator(permission_required('blog.add_menu'))
    def get(self, request):
        form = MenuForm()
        form.post_type = 'post'
        return render(request, 'menus/new.html', {"form": form})

    @method_decorator(permission_required('blog.add_menu'))
    def post(self, request):
        form = MenuForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Menu created successfully.')
            return redirect(reverse('menus'))
        else:
            return render(request, 'menus/new.html', {'form': form})


class Delete(View):
    @method_decorator(permission_required('blog.delete_menu'))
    def post(self, request, menu_id):
        menu = get_object_or_404(Menu, pk=menu_id)
        menu.delete()
        response = {
            'message': "Deleted successfully",
            'id': menu_id,
            'success': True
        }
        return JsonResponse(response)


class Change(View):
    @method_decorator(permission_required('blog.change_menu'))
    def get(self, request, menu_id):
        menu = get_object_or_404(Menu, pk=menu_id)
        form = MenuForm(instance=menu)
        return render(request, 'menus/new.html', {"form": form})

    @method_decorator(permission_required('blog.change_menu'))
    def post(self, request, menu_id):
        menu = get_object_or_404(Menu, pk=menu_id)
        form = MenuForm(data=request.POST, instance=menu)
        if form.is_valid():
            form.save()
            return redirect(reverse('menus'))
        else:
            return render(request, 'menus/new.html', {'form': form})



class View(View):
    @method_decorator(permission_required('blog.view_menu'))
    def get(self, request, menu_id):
        menu = get_object_or_404(Menu, pk=menu_id)
        items = MenuItem.objects.filter(menu=menu, parent=None)
        cat_form = MenuItemFromCategoryForm(menu=menu)
        page_form = MenuItemFromPageForm(menu=menu)
        form = MenuItemLinkForm(menu=menu)
        return render(request, 'menus/show.html',
                      {'menu': menu, 'page_form': page_form, 'cat_form': cat_form, 'form': form, 'items': items})

class ChangeMenuItem(View):
    @method_decorator(permission_required('blog.add_menuitem'))
    def post(self, request, menu_item_id):
        menu_item = get_object_or_404(MenuItem, pk=menu_item_id)
        form = MenuItemForm(data=request.POST, instance=menu_item)
        if form.is_valid():
            item = form.save(commit=False)
            item.save()
            messages.success(request, "Item updated.")
        else:
            messages.error(request, "Some error prevented updating.")
        return redirect(reverse('view_menu', kwargs={'menu_id': menu_item.menu_id}))



class DeleteItem(View):
    @method_decorator(permission_required('blog.delete_menuitem'))
    def post(self, request, menu_item_id):
        menu_item = get_object_or_404(MenuItem, pk=menu_item_id)
        menu_item.delete()
        response = {
            'message': "Deleted successfully",
            'id': menu_item_id,
            'success': True
        }
        return JsonResponse(response)


class ItemFromLink(View):
    def post(self, request, menu_id):
        menu = get_object_or_404(Menu, pk=menu_id)
        form = MenuItemLinkForm(data=request.POST, menu = menu)
        if form.is_valid():
            item = MenuItem(menu_id=menu_id)
            item.title = form.cleaned_data.get('title')
            item.link = form.cleaned_data.get('link')
            item.parent = None
            item.order = 0
            item.save()
            messages.success(request, "Item added successfully.")
        else:
            messages.error(request, "Error occurred while adding item.")
        return redirect(reverse('view_menu', kwargs={'menu_id': menu_id}))
    
class ItemFromPage(View):
    def post(self, request, menu_id):
        menu = get_object_or_404(Menu, pk=menu_id)
        form = MenuItemFromPageForm(data=request.POST, menu = menu)
        if form.is_valid():
            item = MenuItem(menu_id=menu_id)
            page_id = form.cleaned_data.get('page_id')
            page = get_object_or_404(Post, pk=page_id)
            item.title = page.title
            item.link =  reverse('view_blog_post', kwargs={'post_type': 'page', 'post_id': page.id})
            item.parent = None
            item.order = 0
            item.save()
            messages.success(request, "Item added successfully.")
        else:
            messages.error(request, "Error occured while adding item.")
        return redirect(reverse('view_menu', kwargs={'menu_id': menu_id}))
    
    
class ItemFromCat(View):
    def post(self, request, menu_id):
        menu = get_object_or_404(Menu, pk=menu_id)
        form = MenuItemFromCategoryForm(data=request.POST, menu = menu)
        if form.is_valid():
            item = MenuItem(menu_id=menu_id)
            category = get_object_or_404(Category, pk=form.cleaned_data.get('category_id'))
            item.title = category.name
            item.link = reverse('category', kwargs={'category_id': category.id})
            item.parent = None
            item.order = 0
            item.save()
            messages.success(request, "Item added successfully.")
        else:
            messages.error(request, "Error occured while adding item.")
        return redirect(reverse('view_menu', kwargs={'menu_id': menu_id}))