from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View

from admin.forms.categories import CategoryForm
from blog.models import Category


class Index(View):
    @method_decorator(permission_required('blog.view_category'))
    def get(self, request):
        categories = Category.objects.all()
        return render(request, 'categories/index.html', {'categories': categories})


class New(View):
    @method_decorator(permission_required('blog.add_category'))
    def get(self, request):
        form = CategoryForm()
        form.post_type = 'post'
        return render(request, 'categories/new.html', {"form": form})

    @method_decorator(permission_required('blog.add_category'))
    def post(self, request):
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Category created successfully.')
            return redirect(reverse('categories'))
        else:
            return render(request, 'categories/new.html', {'form': form})


class Delete(View):
    @method_decorator(permission_required('blog.add_category'))
    def post(self, request, category_id):
        category = get_object_or_404(Category, pk=category_id)
        category.delete()
        response = {
            'message': "Deleted successfully",
            'id': '{}'.format(category_id),
            'success': True
        }
        return JsonResponse(response)


class Change(View):
    @method_decorator(permission_required('blog.add_category'))
    def get(self, request, category_id):
        category = get_object_or_404(Category, pk=category_id)
        form = CategoryForm(instance=category)
        return render(request, 'categories/new.html', {"form": form})

    @method_decorator(permission_required('blog.add_category'))
    def post(self, request, category_id):
        category = get_object_or_404(Category, pk=category_id)
        form = CategoryForm(data=request.POST, instance=category)
        if form.is_valid():
            form.save()
            return redirect(reverse('categories'))
        else:
            return render(request, 'categories/new.html', {'form': form})
