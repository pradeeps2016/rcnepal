from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View

from admin.forms.advs import AdvForm
from blog.models import Adv


class Index(View):
    @method_decorator(permission_required('blog.view_adv'))
    def get(self, request):
        advs = Adv.objects.all()
        return render(request, 'advs/index.html', {'advs': advs})


class New(View):
    @method_decorator(permission_required('blog.add_adv'))
    def get(self, request):
        form = AdvForm()
        form.post_type = 'post'
        return render(request, 'advs/new.html', {"form": form})

    @method_decorator(permission_required('blog.add_adv'))
    def post(self, request):
        form = AdvForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, 'Adv created successfully.')
            return redirect(reverse('advs'))
        else:
            return render(request, 'advs/new.html', {'form': form})


class Delete(View):
    @method_decorator(permission_required('blog.delete_adv'))
    def post(self, request, adv_id):
        adv = get_object_or_404(Adv, pk=adv_id)
        adv.delete()
        response = {
            'message': "Deleted successfully",
            'id': '{}'.format(adv_id),
            'success': True
        }
        return JsonResponse(response)


class Change(View):
    @method_decorator(permission_required('blog.change_adv'))
    def get(self, request, adv_id):
        adv = get_object_or_404(Adv, pk=adv_id)
        form = AdvForm(instance=adv)
        return render(request, 'advs/new.html', {"form": form})

    @method_decorator(permission_required('blog.change_adv'))
    def post(self, request, adv_id):
        adv = get_object_or_404(Adv, pk=adv_id)
        form = AdvForm(data=request.POST, instance=adv, files=request.FILES)
        if form.is_valid():
            form.save()
            return redirect(reverse('advs'))
        else:
            return render(request, 'advs/new.html', {'form': form})
