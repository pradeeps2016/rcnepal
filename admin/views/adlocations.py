from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View

from admin.forms.adlocations import AdLocationForm
from blog.models import AdLocation


class Index(View):
    @method_decorator(permission_required('blog.view_adlocation'))
    def get(self, request):
        adlocations = AdLocation.objects.all()
        return render(request, 'adlocations/index.html', {'adlocations': adlocations})


class New(View):
    @method_decorator(permission_required('blog.add_adlocation'))
    def get(self, request):
        form = AdLocationForm()
        form.post_type = 'post'
        return render(request, 'adlocations/new.html', {"form": form})

    @method_decorator(permission_required('blog.add_adlocation'))
    def post(self, request):
        form = AdLocationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'AdLocation created successfully.')
            return redirect(reverse('adlocations'))
        else:
            return render(request, 'adlocations/new.html', {'form': form})


class Delete(View):
    @method_decorator(permission_required('blog.delete_adlocation'))
    def post(self, request, adlocation_id):
        adlocation = get_object_or_404(AdLocation, pk=adlocation_id)
        adlocation.delete()
        response = {
            'message': "Deleted successfully",
            'id': adlocation_id,
            'success': True
        }
        return JsonResponse(response)


class Change(View):
    @method_decorator(permission_required('blog.change_adlocation'))
    def get(self, request, adlocation_id):
        adlocation = get_object_or_404(AdLocation, pk=adlocation_id)
        form = AdLocationForm(instance=adlocation)
        return render(request, 'adlocations/new.html', {"form": form})
    @method_decorator(permission_required('blog.change_adlocation'))
    def post(self, request, adlocation_id):
        adlocation = get_object_or_404(AdLocation, pk=adlocation_id)
        form = AdLocationForm(data=request.POST, instance=adlocation)
        if form.is_valid():
            form.save()
            return redirect(reverse('adlocations'))
        else:
            return render(request, 'adlocations/new.html', {'form': form})
