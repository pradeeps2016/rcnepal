from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views import View
from django.views.generic import ListView

from admin.forms.posts import PostForm
from blog.models import Post


class Index(ListView):
    paginate_by = 50
    template_name = 'posts/index.html'

    def dispatch(self, request, post_type, *args, **kwargs):
        self.queryset = Post.objects.filter(post_type=post_type).order_by("-id")
        self.extra_context = {
            'post_type': post_type
        }
        response = super(Index, self).dispatch(request, args, kwargs)
        return response


class New(View):
    def get(self, request, post_type):
        form = PostForm(initial={'post_type': 'post'})
        return render(request, 'posts/new.html', {'form': form, 'post_type': post_type})

    def post(self, request, post_type):
        form = PostForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.views = 0
            post.save()
            messages.success(request, "Post has been created successfully.")
            return redirect(reverse('posts', kwargs={'post_type': post_type}))
        else:
            return render(request, 'posts/new.html', {'form': form, 'post_type': post_type})


class Change(View):
    def get(self, request, post_id):
        content = get_object_or_404(Post, pk=post_id)
        form = PostForm(instance=content)
        return render(request, 'posts/new.html', {'form': form, 'post_type': content.post_type})

    def post(self, request, post_id):
        content = get_object_or_404(Post, pk=post_id)
        form = PostForm(data=request.POST, files=request.FILES, instance=content)
        if form.is_valid():
            form.save()
            messages.success(request, "Post has been updated successfully.")
            return redirect(reverse('posts', kwargs={'post_type': content.post_type}))
        else:
            return render(request, 'posts/new.html', {'form': form, 'post_type': content.post_type})


class Delete(View):

    def post(self, request, post_id):
        content = get_object_or_404(Post, pk=post_id)
        if content.status == 'trashed':
            content.delete()
            message = " has been deleted."
        else:
            content.status = 'trashed'
            content.save()
            message = "has been trashed."

        response = {
            'message': "{} {}".format(content.post_type.capitalize(), message),
            'success': True,
            'id': post_id
        }

        return JsonResponse(response)