import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rcnepal.settings")
django.setup()
from django.utils import timezone
from blog.models import Post

posts = Post.objects.filter(status='scheduled')
if posts:

    for post in posts:

        if post.publish_at < timezone.now():
            post.status = 'published'
            post.save()
            print("{} has been published".format(post.title))