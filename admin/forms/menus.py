from django import forms
from django.forms import ValidationError

from blog.models import Menu, MenuItem, Category, Post


class MenuForm(forms.ModelForm):
    class Meta:
        model = Menu
        fields = ['title', 'location']


class MenuItemLinkForm(forms.ModelForm):
    def __init__(self, menu, *args, **kwargs):
        super(MenuItemLinkForm, self).__init__(*args, **kwargs)
        self.fields['menu_id'] = forms.IntegerField(widget=forms.HiddenInput, initial=menu.id)

    class Meta:
        model = MenuItem
        fields = ['title', 'link']


class MenuItemFromCategoryForm(forms.Form):
    def __init__(self, menu, *args, **kwargs):
        super(MenuItemFromCategoryForm, self).__init__(*args, **kwargs)
        self.fields['menu_id'] = forms.IntegerField(widget=forms.HiddenInput, initial=menu.id)
        self.fields['category_id'] = forms.IntegerField(widget=forms.Select(choices=self.choices()), label="Category")

    def choices(self):
        categories = Category.objects.all()
        return [(r.id, r.name) for r in categories]


class MenuItemFromPageForm(forms.Form):
    def __init__(self, menu, *args, **kwargs):
        super(MenuItemFromPageForm, self).__init__(*args, **kwargs)
        self.fields['menu_id'] = forms.IntegerField(widget=forms.HiddenInput, initial=menu.id)
        self.fields['page_id'] = forms.IntegerField(widget=forms.Select(choices=self.choices()), label='Page')

    def choices(self):
        posts = Post.objects.filter(post_type='page', status='published')
        return [(r.id, r.title) for r in posts]


class MenuItemForm(forms.ModelForm):

    class Meta:
        model = MenuItem
        fields = ['title', 'order', 'parent']

    def clean_parent(self):
        parent = self.cleaned_data.get('parent')
        if parent:
            parent = self.cleaned_data.get('parent')
            title = self.instance.title
            if parent.title == title:
                raise ValidationError('One cannot be parent of self.')
        return parent
