from django import forms

from blog.models import AdLocation


class AdLocationForm(forms.ModelForm):
    class Meta:
        model = AdLocation
        fields = "__all__"
