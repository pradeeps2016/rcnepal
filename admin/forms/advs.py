from django import forms

from blog.models import Adv, AdLocation


class AdvForm(forms.ModelForm):
    locations = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=AdLocation.objects.all())
    class Meta:
        model = Adv
        fields = "__all__"
