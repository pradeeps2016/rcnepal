from django import forms

from blog.models import Media


class MediaForm(forms.ModelForm):
    class Meta:
        model = Media
        fields = ['title', 'description', 'file', 'media_type']


class CkUploaderForm(forms.Form):
    upload = forms.ImageField()
