from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms

from blog.language.language import t
from blog.models import Post, Author, Category


class PostForm(forms.ModelForm):
    authors = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Author.objects.all(), label=t('authors'), required=False)
    post_type = forms.CharField(max_length=100)
    content = forms.CharField(widget=forms.Textarea(attrs={'class': 'content'}), label=t('content'))

    class Meta:
        model = Post
        fields = ['title', 'content', 'excerpt', 'post_type',
                  'publish_at', 'status', 'categories', 'authors', 'priority', 'thumbnail']
        widgets = {
            'excerpt': forms.Textarea(attrs={'cols': 40, 'rows': 20}),
            'categories': forms.CheckboxSelectMultiple(),
            'thumbnail': forms.HiddenInput()
        }
