from django import template
from django.urls import reverse
from django.utils.html import format_html
from blog.language import language
from rcnepal.menu import menus

register = template.Library()


def translate(text: str):
    slug = text.replace(' ', "_").lower()
    return language.t(slug)


@register.simple_tag
def t(index):
    return language.t(index)


def get_children(user, menu):
    chtml = ""
    for child in menu['items']:
        if user.has_perm(child['permission']):
            if 'kwargs' in child:
                url = reverse(child['name'], kwargs=child['kwargs'])
            else:
                url = reverse(child['name'])
            citem = """<li class="nav-item">
                            <a href="{}" class="nav-link active">
                                <i class="text-primary fas fa-{} nav-icon"></i>
                                <p>{}</p>
                            </a>
                        </li>"""
            chtml = chtml + citem.format(url, child['icon'], translate(child['text']))
    return chtml


@register.simple_tag(takes_context=True)
def sidebar_menu(context):
    user = context.request.user
    html = ''
    for menu in menus:
        if user.has_perm(menu['permission']):
            if menu["items"] is not None:
                item = """
                     <li class="nav-item has-treeview menu-close">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon  fas fa-{}"></i>
                        <p>
                           {}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                """
                html = html + item.format(menu['icon'], translate(menu['text']))
                html = html + get_children(user, menu)
                html = html + "</ul></li>"
            else:
                item = """<li class="nav-item">
                                <a href="{}" class="nav-link active">
                                    <i class="text-primary fas fa-{} nav-icon"></i>
                                    <p>{}</p>
                                </a>
                            </li>""".format(reverse(['name']), menu['icon'], menu['text'])
                html = html + item

    return format_html(html)


@register.simple_tag(takes_context=True)
def delete_btn(context, name, obj_id, size=None):
    if context.request.user.has_perm('blog.delete_{}'.format(name)):
        html = format_html('<a title="Delete" class="btn btn-danger confirm-link'
                           ' btn-{}" href="{}"><i class="fas fa-times">'
                           '</i></a>'.format(size, reverse('delete_' + name,
                                                           kwargs={
                                                               name + '_id': obj_id})))
    else:
        html = ""
    return html


@register.simple_tag(takes_context=True)
def del_with_ld(context, name, obj_id, size=None):
    if context.request.user.has_perm('blog.delete_{}'.format(name)):
       html = """
        <a  href="{}" class="btn btn-danger btn-{} ld-ext-right confirm-link">
        Remove
        <span class="ld ld-loader"></span>
        </a>
        """.format(reverse('delete_' + name, kwargs={ name + '_id': obj_id}), size)
    else:
        html = ""
    return format_html(html)


@register.simple_tag(takes_context=True)
def edit_btn(context, name, obj_id, size='sm'):
    if context.request.user.has_perm('blog.change_{}'.format(name)):
        html = format_html('<a title="Change" class="btn btn-success'
                           ' btn-{}" href="{}"><i class="fas fa-pen">'
                           '</i></a>'.format(size, reverse('change_' + name,
                                                           kwargs={
                                                               name + '_id': obj_id})))
    else:
        html = ""
    return html


@register.simple_tag(takes_context=True)
def view_btn(context, name, obj_id, size=None):
    if context.request.user.has_perm('blog.view_{}'.format(name)):
        html = format_html('<a title="View" class="btn btn-primary '
                           'btn-{}" href="{}"><i class="fas fa-eye">'
                           '</i></a>'.format(size, reverse('view_' + name,
                                                           kwargs={
                                                               name + '_id': obj_id})))
    else:
        html = ""
    return html


@register.simple_tag
def add_btn(name, size=None):
    return format_html('<a title="Add New" class="btn btn-primary '
                       'btn-{}" href="{}"><i class="fas fa-plus">'
                       '</i> {}</a>'.format(size, reverse('add_' + name), translate(name)))


@register.simple_tag
def deactivate_btn(name, obj_id, size=None):
    return format_html('<a title="Deactivate" class="btn btn-danger'
                       ' btn-{}" href="{}"><i class="fas fa-ban">'
                       '</i></a>'.format(size, reverse('deactivate_' + name,
                                                       kwargs={
                                                           name + '_id': obj_id})))


@register.simple_tag
def activate_btn(name, obj_id, size=None):
    return format_html('<a title="Activate" class="btn btn-warning'
                       ' btn-{}" href="{}"><i class="fas fa-check">'
                       '</i></a>'.format(size, reverse('activate_' + name,
                                                       kwargs={
                                                           name + '_id': obj_id})))


@register.simple_tag
def approve_btn(name, id, size=None):
    return format_html('<a title="Approve" class="btn btn-success confirm-link'
                       ' btn-{}" href="{}"><i class="fas fa-check">'
                       '</i> {}</a>'.format(size, reverse('approve_' + name,
                                                          kwargs={
                                                              name + '_id': id}), t('Approve')))


@register.simple_tag
def disapprove_btn(name, id, size=None):
    return format_html('<a title="Disapprove" class="btn btn-warning confirm-link'
                       ' btn-{}" href="{}"><i class="fas fa-ban">'
                       '</i> {}</a>'.format(size, reverse('approve_' + name,
                                                          kwargs={
                                                              name + '_id': id}), t('Disapprove')))


@register.filter(name='nd')
def nd(value):
    nep = ["०", "१", "२", "३", "४", "५", "६", "७", "८", "९"]
    num = ""
    for digit in str(value):
        if digit.isdigit():
            num = num + nep[int(digit)]
        else:
            num = num + digit
    return num
