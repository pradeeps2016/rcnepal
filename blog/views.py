from django.shortcuts import render, get_object_or_404

# Create your views here.
from blog.models import Post, Category


def index(request):
    posts = Post.objects.order_by('-id').filter(status='published')[:5]
    return render(request, 'blog/index.html', {'posts': posts})


def single(request, post_type, post_id):
    post = get_object_or_404(Post, pk=post_id)
    post.views = post.views + 1
    post.save()
    return render(request, 'blog/single/post.html'.format(post.post_type), {'post': post})

def category(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    posts = Post.objects.filter(categories__id=category_id, status='published')
    return render(request, 'blog/single/category.html', {'category': category, 'posts': posts})
