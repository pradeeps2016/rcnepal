from django.db import models

# Create your models here.
from django.utils import timezone

from django.utils.html import format_html
from django.utils.timezone import now
from sorl.thumbnail import ImageField

from blog.language.language import t


# class Language(models.Model):
#     name = models.CharField(max_length=255, verbose_name=t('name'))
#     code = models.CharField(max_length=255, verbose_name=t('code'))
#
#     def __str__(self):
#         return self.name


class Author(models.Model):
    name = models.CharField(max_length=500)
    description = models.TextField()
    avatar = models.ImageField()

    def __str__(self):
        return self.name


class Media(models.Model):
    choices = (
        ('document', "Document"),
        ('image', "Image/Video"),
    )
    title = models.CharField(max_length=500, verbose_name=t('title'))
    description = models.CharField(max_length=500, verbose_name=t('description'))
    file = models.FileField(verbose_name=t('file'))
    media_type = models.CharField(choices=choices, max_length=255, verbose_name=t('media_type'))

    def __str__(self):
        return self.title


class Support(models.Model):
    title = models.BooleanField(default=True)
    editor = models.BooleanField(default=True)
    excerpt = models.BooleanField(default=True)
    thumbnail = models.BooleanField(default=True)


class Category(models.Model):
    choices = (
        ('uncategorized', "Uncategorized"),
        ('reviews', "Book Reviews"),
        ('interviews', "Interviews"),
        ('agriculture', "Agriculture"),
        ('literature', "Literature"),
        ('up-coming', "Upcoming"),
        ('life-style', "Life Style"),
        ('news', "News"),
        ('events', "Events"),
        ('science-tech', 'Science & Technology'),
        ('health', "Health"),
        ('modeling', "Modeling")
    )
    name = models.CharField(max_length=100)
    slug = models.CharField(choices=choices, max_length=100)
    show_in_menu = models.BooleanField(default=False)
    menu = models.ManyToManyField("Menu", blank=True)
    posts = models.ManyToManyField('Post', blank=True)

    def __str__(self):
        return self.name


class Post(models.Model):
    choices = (
        ('published', t('Published')),
        ('draft', 'Draft'),
        ('scheduled', t("Scheduled")),
        ('trashed', t("Trashed"))
    )
    post_types = (
        ('post', "Post"),
        ('page', "Page")
    )
    priorities = (
        ('first', t('first')),
        ('second', t('second')),
        ('third', t('third')),
        ('normal', t('normal')),
    )
    title = models.CharField(max_length=500, verbose_name=t('title'))
    content = models.TextField(verbose_name=t('content'))
    post_type = models.CharField(max_length=50, choices=post_types, verbose_name=t('post_type'))
    excerpt = models.CharField(verbose_name=t('excerpt'), max_length=150)
    thumbnail = models.OneToOneField('Media', on_delete=models.SET_NULL, null=True, verbose_name=t('thumbnail'))
    # language = models.ForeignKey('Language', on_delete=models.PROTECT, verbose_name=t('language'))
    status = models.CharField(choices=choices, max_length=100, verbose_name=t('status'))
    authors = models.ManyToManyField(Author, verbose_name=t('authors'), blank=True)
    publish_at = models.DateTimeField(verbose_name=t('publish_at'), default=timezone.now)
    views = models.IntegerField()
    categories = models.ManyToManyField(Category, verbose_name=t('categories'))
    priority = models.CharField(choices=priorities, default='normal', max_length=100, verbose_name=t('priority'))

    def __str__(self):
        return self.title

    @property
    def permalink(self):
        return "/content/{}/{}".format(self.post_type, self.id)

    @property
    def intro(self):
        return self.excerpt[:155] + "..."

    @property
    def thumbnail_url(self):
        if self.thumbnail:
            return self.thumbnail.file.url
        else:
            return None
    @property
    def thumbnail_path(self):
        if self.thumbnail:
            return self.thumbnail.file.path
        else:
            return None

    @property
    def html_content(self):
        return format_html(self.content)


class Menu(models.Model):
    choices = (
        ('primary', "Primary Menu"),
        ('secondary', "Secondary Menu"),
        ('footer', "Footer Menu"),
        ('vertical', "Vertical Menu"),
        ('sidebar', "Sidebar Menu")
    )
    title = models.CharField(max_length=100)
    location = models.CharField(max_length=100, choices=choices)

    # lang = models.ForeignKey('Language', on_delete=models.Empty)

    def __str__(self):
        return self.title


class MenuItem(models.Model):
    title = models.CharField(max_length=100)
    link = models.CharField(max_length=500)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    menu = models.ForeignKey('Menu', on_delete=models.CASCADE)
    order = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    @property
    def has_children(self):
        items = MenuItem.objects.filter(parent=self)
        if items.count() > 0:
            return True
        else:
            return False

    @property
    def children(self):
        return MenuItem.objects.filter(parent_id=self.id)


class AdLocation(models.Model):
    choices = (
        ('before-header', "Before Header"),
        ('after-menu', "After Menu"),
        ('before-post', "Before Post"),
        ('after-post', "Before Post"),
        ('sidebar1', "Sidebar 1"),
        ('sidebar2', "Sidebar 2"),
        ('sidebar3', "Sidebar 3"),
        ('before-widget', "Before Widget"),
        ('after-widget', "After Widget"),
        ('in-footer', "In Footer")
    )
    name = models.CharField(choices=choices, max_length=100, verbose_name=t('name'))
    width = models.CharField(max_length=100, verbose_name=t('width'))
    height = models.CharField(max_length=100, verbose_name=t('height'))
    ad_count = models.IntegerField(verbose_name=t('ad_count'))

    def __str__(self):
        return self.name


class Adv(models.Model):
    name = models.CharField(max_length=100, verbose_name=t('name'))
    image = models.ImageField(verbose_name=t('ad_image'))
    locations = models.ManyToManyField(AdLocation, verbose_name=t('adlocations'))
    display_times = models.IntegerField(verbose_name=t('display_times'))
    load_count = models.IntegerField(blank=True, verbose_name=t('load_count'), default=0)

    def __str__(self):
        return self.name