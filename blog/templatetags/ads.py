from django import template
from django.utils.html import format_html

from blog.models import AdLocation, Adv


register = template.Library()


@register.simple_tag
def ads(location: str, count:int):
    al = AdLocation.objects.filter(name=location).first()
    ads = Adv.objects.filter(locations__name=location)[:count]
    imgs = '<div class="row"><div class="col">'
    if ads.count() > 0:
        for ad in ads:
            imgs = imgs + '<img alt="{}" src="{}" width="{}" height="{}/>'.format(ad.name, ad.image.url, al.width,
                                                                                  al.height)
        return format_html(imgs + '</div></div>')
    else:
        return format_html('')
