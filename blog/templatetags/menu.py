from django import template
from django.utils.html import format_html

from blog.models import Menu, MenuItem

register = template.Library()


@register.simple_tag
def get_menu(location: str):
    menu = Menu.objects.filter(location=location).first()
    items = MenuItem.objects.filter(menu=menu, parent=None)
    lis = ""
    for item in items:
        if item.has_children:
            lis = lis + """<li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown-{}" role="button" 
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {}
                        </a>
                         <div class="dropdown-menu" aria-labelledby="dropdown-{}">""".format(item.id, item.title,
                                                                                             item.id)
            for child in item.children:
                lis = lis + """<a class="dropdown-item" href="{}">{}</a>""".format(child.link, child.title)
            lis = lis + "</div></li>"
        else:
            lis = lis + """<li class="nav-item"><a class="nav-link" href="{}">{}</a></li>""".format(item.link,
                                                                                                    item.title)
    return format_html(lis)
