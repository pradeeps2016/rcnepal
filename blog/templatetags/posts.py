from django import template
from django.templatetags.static import get_media_prefix
from django.utils.html import format_html
from sorl.thumbnail import get_thumbnail

from blog.models import Post

register = template.Library()


@register.simple_tag
def recent_posts(category):
    html = ""
    if category == 'any':
        posts = Post.objects.filter(status='published').order_by('-id')[:5]
    else:
        posts = Post.objects.filter(categories__slug=category, status='published').order_by('-id')[:5]
    for post in posts:
        img = get_thumbnail_url(post.thumbnail_path, size='100x100', crop='center', quality=99)
        html = html + """
                <div class="row" style="margin-top: 5px">
                    <div class="col-2"><img class="sidethumb" title="post-thumb" src="{}"/></div>
                    <div class="col-10"><a class="nav-link" href="{}">{}</a></div>
                </div>""".format(img, post.permalink, post.title)
    return format_html(html)

@register.simple_tag
def top_posts():
    count = 1
    html = ""
    posts = Post.objects.filter(status='published').order_by('-views')[:10]
    for post in posts:
        img = get_thumbnail_url(post.thumbnail_path, '100x100', 'center', 99)
        html = html + """
        <div class="row" style="margin-top: 5px">
            <div class="col-2"><img class="sidethumb" title="post-thumb" src="{}"/></div>
            <div class="col-8"><a class="nav-link" href="{}">{}</a></div>
            <div class="col-2"><span class="badge badge-pill badge-primary float-right">{}</span></div>
        </div>""".format(img ,post.permalink, post.title, count)
        count = count + 1
    return format_html(html)


@register.simple_tag
def get_thumbnail_url(path, size, crop, quality):
    if path is not None:
        return get_thumbnail(path, size, crop=crop, quality=quality).url
    else:
        return ''









