from blog.models import Post


def blog(request):
    return {
        'first_posts': first_posts(),
        'second_posts': second_posts(),
        'posts': posts(),
    }


def first_posts():
    fs = Post.objects.filter(priority='first', post_type='post', status='published').order_by('-id')[:2]
    return fs


def second_posts():
    return Post.objects.filter(priority="second", post_type='post', status='published').order_by('-id')[:4]


def posts():
    return Post.objects.filter(post_type='post', status='published').order_by('-id')[:10]




