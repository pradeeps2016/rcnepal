from django.urls import path

from blog.views import single, index, category

urlpatterns = [
    path('', index, name="home"),
    path('content/<str:post_type>/<int:post_id>', single, name="view_blog_post"),
    path('category/<int:category_id>', category, name='category')
]