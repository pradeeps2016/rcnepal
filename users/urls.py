from django.urls import path

from users.views import groups
from users.views.users import Index, New, Deactivate, Delete, View, Change, Activate

urlpatterns = [
    path('', Index.as_view(), name="users"),
    path('new/', New.as_view(), name="add_user"),
    path('deactivate/<int:user_id>', Deactivate.as_view(), name="deactivate_user"),
    path('activate/<int:user_id>', Activate.as_view(), name="activate_user"),
    path('delete/<int:user_id>', Delete.as_view(), name="delete_user"),
    path('<int:user_id>/', View.as_view(), name="view_user"),
    path('change/<int:user_id>', Change.as_view(), name="change_user"),
    #groups
    path('groups/', groups.Index.as_view(), name="groups"),
    path('groups/new/', groups.New.as_view(), name="add_group"),
    path('groups/<int:group_id>', groups.View.as_view(), name="view_group"),
    path('groups/<int:group_id>/change', groups.Change.as_view(), name='change_group'),
    path('groups/<int:group_id>/delete', groups.Delete.as_view(), name="delete_group")
    ]
