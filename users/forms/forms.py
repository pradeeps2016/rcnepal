from django import forms
from django.contrib.auth.models import Permission, Group

from users.models import User


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password', 'groups']
        widgets = {
            'password': forms.PasswordInput()
        }


class GroupForm(forms.ModelForm):
    _is_new: bool

    def __init__(self, *args, **kwargs):
        self._is_new = kwargs.get('instance') is None
        super().__init__(*args, **kwargs)
        if self._is_new:
            perms = Permission.objects.none()
        else:
            perms = self.instance.permissions
        self.fields['available_permissions'] = forms.ModelMultipleChoiceField(widget=forms.SelectMultiple,
                                                                              required=False,
                                                                              queryset=Permission.objects.get_queryset())
        self.fields['permissions'] = forms.ModelMultipleChoiceField(widget=forms.SelectMultiple, queryset=perms)

    class Meta:
        model = Group
        fields = ['name']

