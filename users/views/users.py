from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import Group
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from users.forms.forms import UserForm
from users.models import User


class Index(View):
    @method_decorator(permission_required('users.view_user'))
    def get(self, request):
        users = User.objects.filter(superuser=False)
        return render(request, 'users/index.html', {'users': users})


class New(View):
    @method_decorator(permission_required('users.add_user'))
    def get(self, request):
        form = UserForm()
        return render(request, 'users/new.html', {'form': form})

    @method_decorator(permission_required('users.add_user'))
    def post(self, request):
        cuser = request.user
        form = UserForm(data=request.POST)
        if form.is_valid():
            password = form.cleaned_data.get('password')
            groups = form.cleaned_data.get('groups')
            user = form.save(commit=False)
            if cuser.is_superuser:
                user.admin = True
            user.set_password(password)
            user.save()
            for gid in groups:
                group = get_object_or_404(Group, pk=gid)
                user.groups.add(int(gid))
            user.save()
            messages.success(request, "User successfully added.")
            return redirect(reverse('users'))
        else:
            messages.error(request, "There is some error")
            return render(request, 'users/new.html', {'form': form})


class Deactivate(View):
    @method_decorator(permission_required('users.deactivate_user'))
    def get(self, request, user_id):
        user = get_object_or_404(User, pk=user_id)
        user.active = False
        user.save()
        messages.success(request, "User deactivation successful.")
        return redirect(reverse('users'))


class Activate(View):
    @method_decorator(permission_required('users.activate_user'))
    def get(self, request, user_id):
        user = get_object_or_404(User, pk=user_id)
        user.active = True
        user.save()
        messages.success(request, "User activation successful.")
        return redirect(reverse('users'))


class Delete(View):
    @method_decorator(permission_required('users.delete_user'))
    def get(self, request, user_id):
        user = get_object_or_404(User, pk=user_id)
        user.delete()
        messages.success(request, "User deleted.")
        return redirect(reverse('users'))


class View(View):
    @method_decorator(permission_required('users.view_user'))
    def get(self, request, user_id):
        user = get_object_or_404(User, pk=user_id)
        groups = user.groups.all()
        return render(request, 'users/show.html', {'user': user, 'groups': groups})


class Change(View):
    @method_decorator(permission_required('users.change_user'))
    def get(self, request, user_id):
        user = get_object_or_404(User, pk=user_id)
        form = UserForm(instance=user)
        return render(request, 'users/new.html', {'form': form})

    @method_decorator(permission_required('users.change_user'))
    def post(self, request, user_id):
        user = get_object_or_404(User, pk=user_id)
        form = UserForm(data=request.POST, instance=user)
        if form.is_valid():
            password = form.cleaned_data.get('password')
            user = form.save()
            user.admin = False
            user.set_password(password)
            user.save()
            messages.success(request, "User successfully updated.")
            return redirect(reverse('users'))
        else:
            messages.error(request, "There is some error")
            return render(request, 'users/new.html', {'form': form})

