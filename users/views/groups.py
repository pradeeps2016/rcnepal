from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.contrib.auth.models import Group
from users.forms.forms import GroupForm


class New(View):
    @method_decorator(permission_required('auth.add_group'))
    def get(self, request):
        form = GroupForm()
        return render(request, 'groups/new.html', {'form': form})

    @method_decorator(permission_required('auth.add_group'))
    def post(self, request):
        permissions = request.POST.getlist('permissions')
        name = request.POST.get('name')
        group = Group(name=name)
        group.save()
        for perm in permissions:
            group.permissions.add(int(perm))
        group.save()
        messages.success(request, "The group has been created.")
        return redirect(reverse('groups'))


class Index(View):
    @method_decorator(permission_required('auth.view_group'))
    def get(self, request):
        groups = Group.objects.all()
        return render(request, 'groups/groups.html', {'groups': groups})


class Change(View):
    @method_decorator(permission_required('auth.change_group'))
    def get(self, request, group_id):
        group = get_object_or_404(Group, pk=group_id)
        form = GroupForm(instance=group)
        return render(request, 'groups/new.html', {'form': form, 'group': group})

    @method_decorator(permission_required('auth.change_group'))
    def post(self, request, group_id):
        group = get_object_or_404(Group, pk=group_id)
        name = request.POST.get('name')
        permissions = request.POST.getlist('permissions')
        group.name = name
        group.permissions.clear()
        for perm in permissions:
            group.permissions.add(int(perm))
        group.save()
        form = GroupForm(instance=group, data=request.POST)
        messages.success(request, "Group updated successfully.")
        return render(request, 'groups/new.html', {'form': form, 'group': group})


class Delete(View):
    @method_decorator(permission_required('auth.delete_group'))
    def get(self, request, group_id):
        group = get_object_or_404(Group, pk=group_id)
        group.delete()
        response = {
            'message': "Group is deleted.",
            'success' : True,
            'id': "item-{}".format(group_id)
        }
        return JsonResponse(response)


class View(View):
    @method_decorator(permission_required('auth.view_group'))
    def get(self, request, group_id):
        group = Group.objects.get(pk=group_id)
        permissions = group.permissions.all()
        return render(request, 'groups/show.html', {'group': group, 'permissions': list(permissions)})
